from django.conf import settings
import os
import torch
from torch import nn
from navec import Navec
from pymystem3 import Mystem
import nltk
from nltk.corpus import stopwords
import string


# Для визуальных эмбеддингов
mobilenet = torch.hub.load('pytorch/vision:v0.10.0', 'mobilenet_v2', pretrained=True)
mobilenet.classifier = nn.Sequential(*list(mobilenet.classifier.children())[:-1])
mobilenet.eval()

# Для текстовых эмбеддингов
filename_navec_data = 'navec_hudlit_v1_12B_500K_300d_100q.tar'
navec = Navec.load(os.path.join(settings.MODEL_DATA_PATH, filename_navec_data))

lemmatizer = Mystem()

nltk.download('stopwords')
russian_stopwords = set(stopwords.words('russian'))
russian_stopwords.add(' ')


def preprocess_text(text):
    tokens = lemmatizer.lemmatize(text)
    tokens = [token.strip() for token in tokens if (token.strip() not in russian_stopwords) and (token.strip() not in string.punctuation)]
    preprocessed_text = ' '.join(tokens)
    return preprocessed_text 
