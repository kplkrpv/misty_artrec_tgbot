from django.conf import settings
import numpy as np
from tgbot_app.preprocessing.embedding_utils import navec
from tgbot_app.preprocessing.embedding_utils import preprocess_text


def default_text_embedding():
    return np.zeros(settings.TEXT_EMBEDDING_SIZE).tolist()


def get_preprocessed_text_and_text_embedding(text):
    preprocessed_text = preprocess_text(text)
    if len(preprocessed_text) == 0:
        return np.zeros(settings.TEXT_EMBEDDING_SIZE), preprocessed_text
    else:
        text_embedding = np.zeros(settings.TEXT_EMBEDDING_SIZE)
        for word in preprocessed_text.split():
            text_embedding += navec.get(word, np.zeros(settings.TEXT_EMBEDDING_SIZE))
        text_embedding = text_embedding / len(text.split())
        return preprocessed_text, text_embedding
