from django.conf import settings
import numpy as np
import skimage
from skimage import transform, io
import torch
from tgbot_app.preprocessing.embedding_utils import mobilenet


def default_visual_embedding():
    return np.zeros(settings.VISUAL_EMBEDDING_SIZE).tolist()


def get_visual_embedding(path):
    img = skimage.img_as_float32(io.imread(path))
    # Случай для len(img.shape) == 2
    if img.shape[2] == 4: # 3689: Абстракция - ['Лазарев Л.К.']
        img = img[:, :, :3]
    img_resized = transform.resize(img, (224, 224))
    img_tensor = torch.from_numpy(img_resized).permute(2, 0, 1).unsqueeze(0)
    img_embedding = mobilenet(img_tensor.float()).detach().numpy()[0] # use .float(): https://stackoverflow.com/questions/56741087/how-to-fix-runtimeerror-expected-object-of-scalar-type-float-but-got-scalar-typ
    return img_embedding
