from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.conf import settings

from tgbot_app.preprocessing.visual_embeddings import default_visual_embedding
from tgbot_app.preprocessing.text_embeddings import default_text_embedding
from tgbot_app.recommender_engine.embedding_generator import default_user_embedding, default_art_embedding, default_visual_pca, default_text_pca


def default_list():
    return ['']


class User(models.Model):
    tg_id = models.PositiveIntegerField(unique=True, default=0)
    nickname = models.CharField(max_length=100, default='', null=True)
    first_name = models.CharField(max_length=100, default='', null=True)
    last_name = models.CharField(max_length=100, default='', null=True)
    embedding = ArrayField(models.FloatField(), default=default_user_embedding)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    # Добавить класс Meta


class Art(models.Model):
    published_on = models.DateTimeField(auto_now_add=True)
    path_to_image = models.ImageField(null=False, upload_to='art_images/')
    name = models.TextField(default='')
    author = ArrayField(models.CharField(max_length=150), default=default_list)
    type = models.CharField(null=False, max_length=50)
    created_place = models.CharField(max_length=200, default='')
    created_date = models.CharField(max_length=150, default='')
    technique = ArrayField(models.CharField(max_length=200), default=default_list)
    description = models.TextField(default='')
    museum = models.CharField(max_length=150, default='')
    likes = models.PositiveIntegerField(default=0)
    views = models.PositiveIntegerField(default=0)
    preprocessed_text = models.TextField(max_length=settings.TEXT_MAX_LEN, default='')
    embedding = ArrayField(models.FloatField(), default=default_art_embedding)
    visual_embedding = ArrayField(models.FloatField(), default=default_visual_embedding)
    text_embedding = ArrayField(models.FloatField(), default=default_text_embedding)
    visual_pca = ArrayField(models.FloatField(), default=default_visual_pca)
    text_pca = ArrayField(models.FloatField(), default=default_text_pca)

    def __str__(self):
        return f'{self.name} - {self.author}'

    # Добавить класс Meta


class Reaction(models.Model):
    reacted_on = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    art = models.ForeignKey(Art, on_delete=models.CASCADE)
    is_liked = models.BooleanField(default=False)
    method = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return f'{self.method}: {self.user} ({self.user.tg_id}) - {self.art.name} ({self.art.id})'
    
    # Добавить класс Meta


# Добавить модели Author, Museum, AuthorMuseum
