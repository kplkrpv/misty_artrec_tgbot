from django.contrib import admin
from tgbot_app.models import User, Art, Reaction

admin.site.register(User)
admin.site.register(Reaction)

@admin.register(Art)
class ArtAdmin(admin.ModelAdmin):
    search_fields = ['id','name']
