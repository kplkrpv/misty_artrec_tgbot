from django.conf import settings
import numpy as np


def default_user_embedding():
    embedding = None
    if settings.RANDOM_MODE == 'bias':
        embedding = np.ones(settings.DIMENSION).tolist()
    return embedding


def default_art_embedding():
    embedding = None
    if settings.RANDOM_MODE == 'bias':
        embedding = np.ones(settings.DIMENSION).tolist()
    return embedding


def default_visual_pca():
    return np.ones(settings.VISUAL_PCA_SIZE).tolist()


def default_text_pca():
    return np.ones(settings.TEXT_PCA_SIZE).tolist()
