import numpy as np
from tgbot_app.getters import get_random_art, get_user_liked_arts_history, get_arts_embeddings, get_user_not_viewed_arts_history, get_art
from tgbot_app.choices import RANDOM_BASED, CONTENT_VISUAL, CONTENT_TEXT


def euclidean_distance(embedding_one, embedding_two):
    return ((embedding_one - embedding_two) ** 2).sum()


class RecommenderEngine:
    def __init__(self):
        self.methods = np.array(['random_based', 'content_visual', 'content_text'])
        self.probs = np.array([0.2, 0.4, 0.4])

    
    def n_not_viewed_arts(self, user_id, n):
        user_not_viewed_arts_ids = get_user_not_viewed_arts_history(user_id)
        user_n_not_viewed_arts_ids = np.random.choice(user_not_viewed_arts_ids, size=n) # это плохо
        return user_n_not_viewed_arts_ids
    

    def inference(self, user_id):
        method = np.random.choice(self.methods, p=self.probs)
        if method == 'random_based':
            art = self.random_based_inference(user_id)
            return art, RANDOM_BASED
        elif method == 'content_visual':
            user_liked_arts_ids = get_user_liked_arts_history(user_id)
            if len(user_liked_arts_ids) == 0:
                art = self.random_based_inference(user_id)
                return art, RANDOM_BASED
            art_anchor_id = np.array([np.random.choice(user_liked_arts_ids)])
            art_candidates_ids = self.n_not_viewed_arts(user_id, 100)
            art_id = self.content_visual_inference(art_anchor_id, art_candidates_ids)
            art = get_art(art_id)
            return art, CONTENT_VISUAL
        elif method == 'content_text':
            user_liked_arts_ids = get_user_liked_arts_history(user_id)
            if len(user_liked_arts_ids) == 0:
                art = self.random_based_inference(user_id)
                return art, RANDOM_BASED
            art_anchor_id = np.array([np.random.choice(user_liked_arts_ids)])
            art_candidates_ids = self.n_not_viewed_arts(user_id, 100)
            art_id = self.content_text_inference(art_anchor_id, art_candidates_ids)
            art = get_art(art_id)
            return art, CONTENT_TEXT

    
    def random_based_inference(self, user_id):
        art = get_random_art(user_id)
        return art


    def content_visual_inference(self, anchor_id, candidates_ids):
        anchor_embedding = get_arts_embeddings(anchor_id, is_visual_embedding=True)
        candidates_embeddings = get_arts_embeddings(candidates_ids, is_visual_embedding=True)
        distances = euclidean_distance(anchor_embedding, candidates_embeddings)
        recommend_object_id = candidates_ids[np.argmin(distances)]
        return recommend_object_id


    def content_text_inference(self, anchor_id, candidates_ids):
        anchor_embedding = get_arts_embeddings(anchor_id, is_text_embedding=True)
        candidates_embeddings = get_arts_embeddings(candidates_ids, is_text_embedding=True)
        distances = euclidean_distance(anchor_embedding, candidates_embeddings)
        recommend_object_id = candidates_ids[np.argmin(distances)]
        return recommend_object_id
