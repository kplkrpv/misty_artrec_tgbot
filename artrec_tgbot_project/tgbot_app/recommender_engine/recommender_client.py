from tgbot_app.recommender_engine.recommender_engine import RecommenderEngine


def get_next_art(user_id):
    recommender_engine = RecommenderEngine()
    art, method = recommender_engine.inference(user_id)
    return art, method
