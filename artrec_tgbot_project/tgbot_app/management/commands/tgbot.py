from crypt import METHOD_SHA256
from django.core.management.base import BaseCommand
from django.conf import settings
from telegram import Bot, Update
from telegram import ParseMode
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, KeyboardButton, ReplyKeyboardMarkup
from telegram.ext import Updater, CallbackContext
from telegram.ext import MessageHandler, CommandHandler, CallbackQueryHandler
from telegram.ext import Filters
from telegram.utils.request import Request
from tgbot_app.models import User, Art, Reaction
from tgbot_app.recommender_engine.recommender_client import get_next_art
import random
import emoji
# import joblib
# from sklearn.neighbors import NearestNeighbors
import numpy as np


# nrst_nbrs_model_path = '/home/nyht/misty/misty_project/recartbot/models/nrst_nbrs_model.sav'
# nrst_nbrs = joblib.load(nrst_nbrs_model_path)


BUTTON_RECS = 'Арт-флоу'


CALLBACK_BUTTON_MORE = 'callback_button_more'
CALLBACK_BUTTON_HIDE = 'callback_button_hide'
CALLBACK_BUTTON_EMPTY_LIKE = 'callback_button_empty_like'
CALLBACK_BUTTON_LIKE = 'callback_button_like'
CALLBACK_BUTTON_PREV = 'callback_button_prev'
CALLBACK_BUTTON_NEXT = 'callback_button_next'


TITLES = {
    CALLBACK_BUTTON_MORE: 'Подробнее',
    CALLBACK_BUTTON_HIDE: 'Скрыть',
    CALLBACK_BUTTON_EMPTY_LIKE: emoji.emojize(':white_heart:'),
    CALLBACK_BUTTON_LIKE: emoji.emojize(':red_heart:'),
    CALLBACK_BUTTON_PREV: emoji.emojize(':left_arrow:'),
    CALLBACK_BUTTON_NEXT: emoji.emojize(':right_arrow:')
}


def log_errors(f):
    def inner(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception as e:
            error_message = f'Произошла ошибка: {e}'
            print(error_message)
            raise e
    return inner


def get_base_inline_keyboard(is_liked):
    if is_liked:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_MORE], callback_data=CALLBACK_BUTTON_MORE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_LIKE], callback_data=CALLBACK_BUTTON_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    else:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_MORE], callback_data=CALLBACK_BUTTON_MORE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_EMPTY_LIKE], callback_data=CALLBACK_BUTTON_EMPTY_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    return InlineKeyboardMarkup(keyboard)


def get_inline_keyboard_after_more(is_liked):
    if is_liked:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_HIDE], callback_data=CALLBACK_BUTTON_HIDE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_LIKE], callback_data=CALLBACK_BUTTON_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    else:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_HIDE], callback_data=CALLBACK_BUTTON_HIDE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_EMPTY_LIKE], callback_data=CALLBACK_BUTTON_EMPTY_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    return InlineKeyboardMarkup(keyboard)


def get_inline_keyboard_after_like(is_more):
    if is_more:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_HIDE], callback_data=CALLBACK_BUTTON_HIDE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_LIKE], callback_data=CALLBACK_BUTTON_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    else:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_MORE], callback_data=CALLBACK_BUTTON_MORE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_LIKE], callback_data=CALLBACK_BUTTON_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    return InlineKeyboardMarkup(keyboard)


def get_inline_keyboard_after_dislike(is_more):
    if is_more:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_HIDE], callback_data=CALLBACK_BUTTON_HIDE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_EMPTY_LIKE], callback_data=CALLBACK_BUTTON_EMPTY_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    else:
        keyboard = [
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_MORE], callback_data=CALLBACK_BUTTON_MORE)],
            [InlineKeyboardButton(TITLES[CALLBACK_BUTTON_EMPTY_LIKE], callback_data=CALLBACK_BUTTON_EMPTY_LIKE)],
            [
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_PREV], callback_data=CALLBACK_BUTTON_PREV),
                InlineKeyboardButton(TITLES[CALLBACK_BUTTON_NEXT], callback_data=CALLBACK_BUTTON_NEXT),
            ]
        ]
    return InlineKeyboardMarkup(keyboard)


def get_base_reply_keyboard():
    keyboard = [
        [
            KeyboardButton(BUTTON_RECS)
        ]
    ]
    return ReplyKeyboardMarkup(
        keyboard=keyboard,
        resize_keyboard=True
    )


@log_errors
def keyboard_callback_handler(update: Update, context: CallbackContext):
    query = update.callback_query
    callback_button_data = query.data

    user_id = update.effective_message.chat_id
    user_object = User.objects.filter(tg_id__exact=user_id).get()
    user_id = user_object.id
    
    art_id = context.user_data['rec_art_id']
    random_art = Art.objects.filter(id__exact=art_id).get()
    text = query.message.caption

    if callback_button_data == CALLBACK_BUTTON_MORE:
        context.user_data['is_more'] = True
        is_liked = context.user_data['is_liked']

        art_name = random_art.name
        art_author = random_art.author
        if art_author == '':
            art_author = 'Информация отсутствует'
        else:
            art_author = ', '.join(art_author)
        art_museum = random_art.museum
        art_type = random_art.type
        art_created_place = random_art.created_place
        art_created_date = random_art.created_date
        art_technique = random_art.technique
        if art_technique == '':
            art_technique = 'Информация отсутствует'
        else:
            art_technique = ', '.join(art_technique)
        art_description = random_art.description
        more_text_art = f'*Название:* {art_name}\n*Автор:* {art_author}\n*Музей:* {art_museum}\n*Вид:* {art_type}\n*Место создания:* {art_created_place}\n*Время создания:* {art_created_date}\n*Техника:* {art_technique}\n\n*Описание:* {art_description}'

        context.user_data['rec_art_caption'] = more_text_art

        query.edit_message_caption(
            caption=more_text_art,
            reply_markup=get_inline_keyboard_after_more(is_liked),
            parse_mode=ParseMode.MARKDOWN
        )
    elif callback_button_data == CALLBACK_BUTTON_HIDE:
        context.user_data['is_more'] = False
        is_liked = context.user_data['is_liked']

        art_name = random_art.name
        art_author = random_art.author
        if art_author == '':
            art_author = 'Информация отсутствует'
        else:
            art_author = ', '.join(art_author)
        art_museum = random_art.museum
        text_art = f'*Название:* {art_name}\n*Автор:* {art_author}\n*Музей:* {art_museum}'

        context.user_data['rec_art_caption'] = text_art

        query.edit_message_caption(
            caption=text_art,
            reply_markup=get_base_inline_keyboard(is_liked),
            parse_mode=ParseMode.MARKDOWN
        )
    elif callback_button_data == CALLBACK_BUTTON_EMPTY_LIKE:
        context.user_data['is_liked'] = True
        is_more = context.user_data['is_more']

        reaction = Reaction.objects.filter(user__exact=user_id, art__exact=art_id).get()
        reaction.is_liked = True
        reaction.save()

        random_art.likes += 1
        random_art.save()

        # query.edit_message_caption(
        #     caption=text,
        #     reply_markup=get_inline_keyboard_after_like(is_more),
        # )
        query.edit_message_reply_markup(
            reply_markup=get_inline_keyboard_after_like(is_more)
        )
    elif callback_button_data == CALLBACK_BUTTON_LIKE:
        context.user_data['is_liked'] = False
        is_more = context.user_data['is_more']

        reaction = Reaction.objects.filter(user__exact=user_id, art__exact=art_id).get()
        reaction.is_liked = False
        reaction.save()

        random_art.likes -= 1
        random_art.save()

        # query.edit_message_caption(
        #     caption=text,
        #     reply_markup=get_inline_keyboard_after_dislike(is_more),
        # )
        query.edit_message_reply_markup(
            reply_markup=get_inline_keyboard_after_dislike(is_more)
        )
    elif callback_button_data == CALLBACK_BUTTON_NEXT:
        if context.user_data['current_transaction_index'] == 0:
            context.user_data['is_liked'] = False

            # query.edit_message_caption(
            #     caption=text,
            # )

            query.delete_message()
            
            rec_art, rec_art_id, rec_art_caption, rec_art_photo, method = make_art_recommendation(user_object)

            query.message.reply_photo(
                caption=rec_art_caption,
                photo=rec_art_photo,
                reply_markup=get_base_inline_keyboard(context.user_data['is_liked']),
                parse_mode=ParseMode.MARKDOWN
            )

            context.user_data['rec_art_id'] = rec_art_id
            context.user_data['rec_art_caption'] = rec_art_caption

            Reaction(
                user = user_object,
                art = rec_art,
                is_liked = False,
                method = method
            ).save()

            rec_art.views += 1
            rec_art.save()
        else:
            context.user_data['current_transaction_index'] -= 1
            
            user_reactions = Reaction.objects.filter(user__exact=user_id).order_by('-reacted_on')
            prev_id_reaction = user_reactions.values_list('id', flat=True)[context.user_data['current_transaction_index']]

            prev_reaction = Reaction.objects.filter(id__exact=prev_id_reaction).get()

            prev_rec_art = prev_reaction.art
            prev_rec_art_id = prev_rec_art.pk
            prev_rec_art_name = prev_rec_art.name
            prev_rec_art_author = prev_rec_art.author
            if prev_rec_art_author == '':
                prev_rec_art_author = 'Информация отсутствует'
            else:
                prev_rec_art_author = ', '.join(prev_rec_art_author)
            prev_rec_art_museum = prev_rec_art.museum
            prev_rec_art_caption = f'*Название:* {prev_rec_art_name}\n*Автор:* {prev_rec_art_author}\n*Музей:* {prev_rec_art_museum}'

            full_path_to_rec_art_photo = f'/home/misty/misty/misty_project/misty_artrec_tgbot/artrec_tgbot_project/media/{prev_rec_art.path_to_image}'
            prev_rec_art_photo = open(full_path_to_rec_art_photo, 'rb')

            # query.edit_message_caption(
            #     caption=context.user_data['rec_art_caption']
            # )

            query.delete_message()

            context.user_data['rec_art_id'] = prev_rec_art_id
            context.user_data['is_more'] = False
            context.user_data['is_liked'] = prev_reaction.is_liked
            context.user_data['rec_art_caption'] = prev_rec_art_caption

            query.message.reply_photo(
                caption=prev_rec_art_caption,
                photo=prev_rec_art_photo,
                reply_markup=get_base_inline_keyboard(context.user_data['is_liked']),
                parse_mode=ParseMode.MARKDOWN
            )
    elif callback_button_data == CALLBACK_BUTTON_PREV:
        context.user_data['current_transaction_index'] += 1

        user_reactions = Reaction.objects.filter(user_id__exact=user_id).order_by('-reacted_on')
        prev_id_reaction = user_reactions.values_list('id', flat=True)[context.user_data['current_transaction_index']]

        prev_reaction = Reaction.objects.filter(id__exact=prev_id_reaction).get()

        prev_rec_art = prev_reaction.art
        prev_rec_art_id = prev_rec_art.pk
        prev_rec_art_name = prev_rec_art.name
        prev_rec_art_author = prev_rec_art.author
        if prev_rec_art_author == '':
            prev_rec_art_author = 'Информация отсутствует'
        else:
            prev_rec_art_author = ', '.join(prev_rec_art_author)
        prev_rec_art_museum = prev_rec_art.museum
        prev_rec_art_caption = f'*Название:* {prev_rec_art_name}\n*Автор:* {prev_rec_art_author}\n*Музей:* {prev_rec_art_museum}'

        full_path_to_rec_art_photo = f'/home/misty/misty/misty_project/misty_artrec_tgbot/artrec_tgbot_project/media/{prev_rec_art.path_to_image}'
        prev_rec_art_photo = open(full_path_to_rec_art_photo, 'rb')

        # query.edit_message_caption(
        #     caption=context.user_data['rec_art_caption'],
        # )

        query.delete_message()

        context.user_data['rec_art_id'] = prev_rec_art_id
        context.user_data['is_more'] = False
        context.user_data['is_liked'] = prev_reaction.is_liked
        context.user_data['rec_art_caption'] = prev_rec_art_caption

        query.message.reply_photo(
            caption=prev_rec_art_caption,
            photo=prev_rec_art_photo,
            reply_markup=get_base_inline_keyboard(context.user_data['is_liked']),
            parse_mode=ParseMode.MARKDOWN
        )


@log_errors
def make_art_recommendation(user_object):
    user_id = user_object.id

    rec_art, method = get_next_art(user_id)
   
    rec_art_id = rec_art.id
    rec_art_name = rec_art.name
    rec_art_author = rec_art.author
    if rec_art_author == '':
        rec_art_author = 'Информация отсутствует'
    else:
        rec_art_author = ', '.join(rec_art_author)
    rec_art_museum = rec_art.museum
    rec_art_caption = f'*Название:* {rec_art_name}\n*Автор:* {rec_art_author}\n*Музей:* {rec_art_museum}'
    full_path_to_rec_art_photo = f'/home/misty/misty/misty_project/misty_artrec_tgbot/artrec_tgbot_project/media/{rec_art.path_to_image}'
    rec_art_photo = open(full_path_to_rec_art_photo, 'rb')
    return rec_art, rec_art_id, rec_art_caption, rec_art_photo, method


@log_errors
def send_recommended_art(update: Update, context: CallbackContext):
    pass


@log_errors
def do_echo(update: Update, context: CallbackContext):
    user_id = update.message.chat_id
    text = update.message.text

    p, _ = User.objects.get_or_create(
        tg_id=user_id,
        defaults={
            'nickname': update.message.from_user.username,
            'first_name': update.message.from_user.first_name,
            'last_name': update.message.from_user.last_name
        }
    )
    
    user_object = User.objects.filter(tg_id__exact = user_id).get()

    if text == BUTTON_RECS:
        rec_art, rec_art_id, rec_art_caption, rec_art_photo, method = make_art_recommendation(user_object)

        context.user_data['rec_art_id'] = rec_art_id
        context.user_data['is_more'] = False
        context.user_data['is_liked'] = False
        context.user_data['current_transaction_index'] = 0
        context.user_data['rec_art_caption'] = rec_art_caption

        update.message.reply_photo(
            caption=rec_art_caption,
            photo=rec_art_photo,
            reply_markup=get_base_inline_keyboard(context.user_data['is_liked']),
            parse_mode=ParseMode.MARKDOWN
        )

        Reaction(
            user = user_object,
            art = rec_art,
            is_liked = False,
            method = method,
        ).save()

        rec_art.views += 1
        rec_art.save()

        # Добавить увиличение счетчика просмотров для данного мема


@log_errors
def do_start(update: Update, context: CallbackContext):
    user_id = update.message.chat_id
    p, _ = User.objects.get_or_create(
        tg_id=user_id,
        defaults={
            'nickname': update.message.from_user.username,
            'first_name': update.message.from_user.first_name,
            'last_name': update.message.from_user.last_name
        }
    )

    if _ == True:
        update.message.reply_text(
            text = 'Привет-привет, добро пожаловать в мир изобразительного искусства!'
        )


@log_errors
def do_menu(update: Update, context: CallbackContext):
    update.message.reply_text(
        text=f'Пойдем просвещаться! {emoji.emojize(":sparkles:")}',
        reply_markup=get_base_reply_keyboard()
    )


class Command(BaseCommand):
    def handle(self, *args, **options):
        request = Request(
            connect_timeout=0.5,
            read_timeout=1.0
        )
        bot = Bot(
            request=request,
            token=settings.TGBOT_TOKEN
        )
        updater = Updater(
            bot=bot,
            use_context=True
        )

        start_handler = CommandHandler('start', do_start)
        menu_handler = CommandHandler('menu', do_menu)
        message_handler = MessageHandler(Filters.text, do_echo)
        buttons_handler = CallbackQueryHandler(callback=keyboard_callback_handler, pass_chat_data=True)
        
        updater.dispatcher.add_handler(start_handler)
        updater.dispatcher.add_handler(menu_handler)
        updater.dispatcher.add_handler(message_handler)
        updater.dispatcher.add_handler(buttons_handler)

        # запуск обработки входящих сообщений
        updater.start_polling()
        updater.idle()

