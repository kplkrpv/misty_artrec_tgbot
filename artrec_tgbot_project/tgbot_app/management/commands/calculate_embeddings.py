from django.core.management.base import BaseCommand
import numpy as np
from PIL import Image
from tgbot_app.models import Art
from tgbot_app.preprocessing.visual_embeddings import get_visual_embedding
from tgbot_app.preprocessing.text_embeddings import get_preprocessed_text_and_text_embedding
from tgbot_app.setters import set_visual_embedding, set_preprocessed_text_and_text_embedding


Image.MAX_IMAGE_PIXELS = None

class Command(BaseCommand):
    def handle(self, *args, **options):
        arts = Art.objects.all().order_by('id')
        for art in arts:
            print(f'{art.id}: {art}')
            art_id = art.id
            art_visual_embedding = np.array(art.visual_embedding)
            art_text_embedding = np.array(art.text_embedding)
            if (art_visual_embedding == 0).all():
                art_image_path = art.path_to_image.path
                art_visual_embedding = get_visual_embedding(art_image_path)
                set_visual_embedding(art_id, art_visual_embedding)
                print('Visual embedding: OK')
            if (art_text_embedding == 0).all():
                text = art.name + ' ' + ', '.join(art.author) + ' ' + art.type + ' ' +  art.created_place + ' ' +  art.created_date + ' ' +  ', '.join(art.technique) + ' ' +  art.description + ' ' +  art.museum
                preprocessed_text, art_text_embedding = get_preprocessed_text_and_text_embedding(text)
                set_preprocessed_text_and_text_embedding(art_id, preprocessed_text, art_text_embedding)
                print('Text embedding: OK')
            print()
