from django.core.management.base import BaseCommand
from django.conf import settings
import requests
import urllib.request
from urllib.error import HTTPError, URLError
import time
from tgbot_app.models import Art


def upload_art_data(art, art_museum, art_type, art_path_to_image):
    if 'name' in art.keys():
        art_name = art['name']
    else:
        art_name = ''
    if 'authors' in art.keys():
        art_author = art['authors']
    else:
        art_author = ['']
    if 'productionPlace' in art.keys():
        art_created_place = art['productionPlace']
    else:
        art_created_place = ''
    if 'periodStr' in art.keys():
        art_created_date = art['periodStr']
    else:
        art_created_date = ''
    if 'technologies' in art.keys():
        art_technique = art['technologies']
    else:
        art_technique = ['']
    if 'description' in art.keys():
        art_description = art['description']
    else:
        art_description = ''
    Art(
        path_to_image = art_path_to_image,
        name = art_name,
        author = art_author,
        type = art_type,
        created_place = art_created_place,
        created_date = art_created_date,
        technique = art_technique,
        description = art_description,
        museum = art_museum
    ).save()


class Command(BaseCommand):
    def handle(self, *args, **options):
        headers = {'X-API-KEY': settings.OPENDATA_KEY}
        if settings.UPLOAD_MODE == 'part':
            art_number = 1
            for museum in settings.MUSEUMS:
                print(museum)
                for i_type in settings.TYPES:
                    print(i_type)
                    link = 'https://opendata.mkrf.ru/v2/museum-exhibits/$?f={"data.museum.name":{"$contain":"' + f'{settings.MUSEUMS_FILTER[museum]}' + '"},"data.typology.name":{"$contain":"' + f'{settings.TYPES_FULL[i_type]}' + '"}}&l=100'
                    response = requests.get(link, headers=headers)
                    arts = response.json()['data']
                    for art_data in arts:
                        art = art_data['data']
                        if art['museum']['name'].lower() in [museum_name.lower() for museum_name in settings.MUSEUMS_FULL[museum]]: # так как в link идет неполное название музея, то могут быть найдены музеи не из списка
                            if 'images' in art.keys(): # рассматриваем только предметы ИИ, имеющие ссылки на изображения
                                if len(art['images']) == 1: # рассматриваем только предметы ИИ с одним изображением
                                    art_image_url = art['images'][0]['url']
                                    art_full_path_to_image = f'/home/misty/misty/misty_project/misty_artrec_tgbot/artrec_tgbot_project/media/art_images/{art_number}.jpg'
                                    print(f'{art_number}: {art_image_url}')
                                    try:
                                        urllib.request.urlretrieve(art_image_url, art_full_path_to_image)
                                        art_path_to_image = f'art_images/{art_number}.jpg'
                                        upload_art_data(art, museum, i_type, art_path_to_image)
                                        art_number += 1
                                    except (HTTPError, URLError) as error: # проблемы: 1. не все ссылки открываются или доступны; 2. превышение лимита количества запросов
                                        try:
                                            print(f'>>> {error.reason}')
                                            if error.reason == 'Bad Request':
                                                print('> BR')
                                            elif error.reason == 'Not Found':
                                                print('> NF')
                                            else:
                                                print('> Sleep')
                                                time.sleep(30)
                                                urllib.request.urlretrieve(art_image_url, art_full_path_to_image)
                                                upload_art_data(art, museum, i_type, art_path_to_image)
                                            art_number += 1
                                        except (HTTPError, URLError) as error:
                                            if error.reason == 'Bad Request':
                                                print('> BR')
                                            elif error.reason == 'Not Found':
                                                print('> NF')
                                            else:
                                                print('> Smth')
                                            art_number += 1
                print()
