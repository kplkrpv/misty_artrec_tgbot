import random
import numpy as np
from django.db.models import Q
from tgbot_app.models import User, Art, Reaction


def get_random_art(user_id):
    user_viewed_arts = Reaction.objects.filter(user__exact=user_id).values_list('art', flat=True)
    user_not_viewed_arts = Art.objects.exclude(id__in=user_viewed_arts).values_list('id', flat=True)
    random_art_id = random.choice(user_not_viewed_arts)
    return Art.objects.filter(id__exact=random_art_id).get()


def get_user_liked_arts_history(user_id):
    user = User.objects.filter(pk=user_id)[0]

    query = Q(user=user)
    query = query & Q(is_liked=True)
    reactions = Reaction.objects.filter(query).order_by('-reacted_on')
    
    arts_ids = np.array(reactions.values_list('art__pk', flat=True))
    
    return arts_ids


def get_user_not_viewed_arts_history(user_id):
    user_viewed_arts = Reaction.objects.filter(user__exact=user_id).values_list('art', flat=True)
    user_not_viewed_arts_ids = Art.objects.exclude(id__in=user_viewed_arts).values_list('id', flat=True)
    return user_not_viewed_arts_ids


def get_arts_embeddings(arts_ids, is_visual_embedding=False, is_text_embedding=False):
    arts = Art.objects.filter(id__in=arts_ids).values_list('id', flat=True)
    if is_visual_embedding:
        arts_visual_embeddings = arts.values_list('visual_embedding', flat=True)
        return np.array(arts_visual_embeddings)
    elif is_text_embedding:
        arts_text_embeddings = arts.values_list('text_embedding', flat=True)
        return np.array(arts_text_embeddings)


def get_art(art_id):
    return Art.objects.filter(id__exact=art_id).get()
