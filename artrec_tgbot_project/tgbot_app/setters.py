from tgbot_app.models import Art


def set_visual_embedding(art_id, art_visual_embedding):
    art = Art.objects.filter(pk=art_id)[0]
    art.visual_embedding = art_visual_embedding.tolist()
    art.save()


def set_preprocessed_text_and_text_embedding(art_id, art_preprocessed_text, art_text_embedding):
    art = Art.objects.filter(pk=art_id)[0]
    art.preprocessed_text = art_preprocessed_text
    art.text_embedding = art_text_embedding.tolist()
    art.save()
