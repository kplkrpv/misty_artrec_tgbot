Django==4.0.1
psycopg2==2.9.3
numpy==1.22.1
Pillow== 9.0.0
requests==2.27.1
python-telegram-bot==13.10
scikit-image==0.19.1
# pip install torch==1.6.0+cpu torchvision==0.7.0+cpu -f https://download.pytorch.org/whl/torch_stable.html
navec==0.10.0
pymystem3==0.2.0
nltk==3.6.7
emoji==1.6.3
gunicorn==20.1.0
